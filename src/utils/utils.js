export function concatClasses(...props) {
  return props.reduce(
    (result, current) =>
      !!current ? `${result ? result + " " : ""}${current}` : result,
    ""
  );
}
