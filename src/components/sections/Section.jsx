import React from "react";
import "./Section.scss";
import { concatClasses } from "../../utils/utils";

export function Section({ id, children, className = "" }) {
  return (
    <section className={concatClasses("section", className)} id={id}>
      {children}
    </section>
  );
}
