import React from "react";

import "./SectionGameTypes.scss";

import icon1 from "../../../images/gameTypes/icon_1.jpg";
import icon2 from "../../../images/gameTypes/icon_2.jpg";
import icon3 from "../../../images/gameTypes/icon_3.jpg";
import icon4 from "../../../images/gameTypes/icon_4.jpg";
import icon5 from "../../../images/gameTypes/icon_5.jpg";
import icon6 from "../../../images/gameTypes/icon_6.jpg";

import { Section } from "../Section";

export function SectionGameTypes() {
  return (
    <Section className="section_game-types">
      <h2>МЫ ОРГАНИЗОВЫВАЕМ</h2>
      <p>В Мафию можно играть на любом празднике в любое время года.</p>
      <div className="game-types__item">
        <img src={icon1} alt="Клубные игры" />
        <span>Клубные игры</span>
      </div>
      <div className="game-types__item">
        <img src={icon2} alt="English Маfia" />
        <span>English Маfia</span>
      </div>
      <div className="game-types__item">
        <img src={icon3} alt="Детская мафия" />
        <span>Детская мафия</span>
      </div>
      <div className="game-types__item">
        <img src={icon4} alt="Частные вечеринки" />
        <span>Частные вечеринки</span>
      </div>
      <div className="game-types__item">
        <img src={icon5} alt="Дни рождения" />
        <span>Дни рождения</span>
      </div>
      <div className="game-types__item">
        <img src={icon6} alt="Корпоративы" />
        <span>Корпоративы</span>
      </div>
      <a className="link section_game-types__link" href="#section_form">
        ЗАКАЗАТЬ МЕРОПРИЯТИЕ
      </a>
    </Section>
  );
}
