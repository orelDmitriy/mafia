import React from "react";

import "./SectionAbout.scss";

import background from "../../../images/about/background.jpg";

import { Section } from "../Section";

export function SectionAbout() {
  return (
    <Section className="section_about">
      <div className="about__sale sale">
        <p className="sale__title">А так же вы получите!</p>
        <p className="sale__description">
          Скидку 20% на следующий заказ мафии, если оставите видео отзыв
        </p>
      </div>
      <h2>О нас</h2>
      <p>
        Мафклуб "PATRON" – дружелюбный клуб в городе Минске, в котором играют в
        Мафию по классическим правилам.
      </p>
      <p>У нас живое общение, новые знакомства, адреналин и эмоции.</p>
      <p>Приходите за хорошим настроением!</p>
      <img src={background} alt="Мафия на корпоратив" />
      <a className="link section_about__link" href="#section_form">
        ЗАПИСАТЬСЯ НА ИГРУ
      </a>
    </Section>
  );
}
