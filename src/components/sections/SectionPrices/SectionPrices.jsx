import React from "react";

import icon1 from "../../../images/prices/icon_1.jpg";
import icon2 from "../../../images/prices/icon_2.jpg";
import icon3 from "../../../images/prices/icon_3.jpg";

import { Section } from "../Section";

import "./SectionPrices.scss";
import { PriceItem } from "./PriceItem/PriceItem";

export function SectionPrices() {
  return (
    <Section className="section_prices">
      <h2>стоимость незабываемых ярких эмоций</h2>
      <p>
        Компания от 7 до 14 человек 4 часа игры Одна игра длится 30-40 минут
      </p>
      <PriceItem
        image={icon1}
        type="Базовый"
        price="250 byn"
        list={[
          "профессиональный ведущий",
          "реквизит мафии",
          "антуражный реквизит",
          "музыкальное сопровождение",
          "призы лучшим игрокам",
        ]}
      />
      <PriceItem
        image={icon2}
        type="оптимальный"
        price="300 byn"
        list={[
          "профессиональный ведущий",
          "реквизит мафии",
          "антуражный реквизит",
          "музыкальное сопровождение",
          "призы лучшим игрокам",
          "подарок для компании",
          "накопительная карта клиента",
        ]}
      />
      <PriceItem
        image={icon3}
        type="vip"
        price="380 byn"
        list={[
          "профессиональный ведущий",
          "реквизит мафии",
          "антуражный реквизит",
          "музыкальное сопровождение",
          "призы лучшим игрокам",
          "подарок для компании",
          "накопительная карта клиента",
          "фотограф",
          "скидка 20% для каждого в Клубе",
        ]}
      />
    </Section>
  );
}
