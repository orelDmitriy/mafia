import React from "react";

import "./PriceItem.scss";

export function PriceItem({ image, type, price, list }) {
  return (
    <div className="price-item">
      <div className="price-item__card">
        <p className="price-item__type">{type}</p>
        <p className="price-item__price">{price}</p>
        <img src={image} alt={type} />
        <a className="link price-item__link" href="#section_form">
          Заказать
        </a>
        <ul className="price-item__offers">
          {list.map((element) => (
            <li key={element}>{element}</li>
          ))}
        </ul>
      </div>
    </div>
  );
}
