import React from "react";
import ReCAPTCHA from "react-google-recaptcha";

import { Section } from "../Section";

import "./SectionForm.scss";

function onChange(value) {
  console.log("Captcha value:", value);
}

export function SectionForm() {
  return (
    <Section id="section_form" className="section_form">
      <h2>хотите особенное мероприятие?</h2>
      <form onSubmit={() => {}}>
        <input type="tel" placeholder="Телефон" name="phone" />
        <label htmlFor="where">
          Где вам ответить? <span>*</span>
        </label>
        <select name="where" defaultValue="any">
          <option value="any" disabled>
            -Выберите ответ-
          </option>
          <option value="Telegram">Telegram</option>
          <option value="Viber">Viber</option>
          <option value="WhatsApp">WatsApp</option>
          <option value="Phone">По телефону</option>
        </select>
        <input type="textarea" placeholder="Сообщение" />
        <ReCAPTCHA sitekey="Your client site key" onChange={onChange} />
        <button onClick={() => {}}>Отправить</button>
      </form>
    </Section>
  );
}
