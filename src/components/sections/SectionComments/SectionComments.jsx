import React from "react";

import icon1 from "../../../images/comments/icon_1.jpg";
import icon2 from "../../../images/comments/icon_2.jpg";
import icon3 from "../../../images/comments/icon_3.jpg";
import icon4 from "../../../images/comments/icon_4.jpg";

import "./SectionComments.scss";

import { Section } from "../Section";
import { Carousel } from "../../Carousel/Carousel";

export function SectionComments() {
  return (
    <Section className="section_comments">
      <h2>отзывы клиентов о нас</h2>
      <Carousel>
        <div className="section_comments__carousel-item carousel-item">
          <img src={icon1} alt="Ксения" className="carousel-item__image" />
          <span className="carousel-item__name">Ксения</span>
          <p className="carousel-item__comment">
            Несколько раз приглашали Екатерину в качестве проведения тим
            билдинге у нас в ИТ компании.Атмосфера шикарная, Екатерина нашла
            общий язык со всеми ребятами, обычно планируем 4 часа игры до часто
            затягиваем и дополуночи , так как ребята втягиваются , как Тим
            билдинг или мини-корпоратив-рекомендую (у нас 65 человек в
            компании-играет обычно 25-30:)
          </p>
        </div>
        <div className="section_comments__carousel-item carousel-item">
          <img src={icon2} alt="Виктория" className="carousel-item__image" />
          <span className="carousel-item__name">Виктория</span>
          <p className="carousel-item__comment">
            Вечер был просто Великолепный! Обстановка, Обслуживание, Друзья -
            Высший пилотаж!!! Вчеринка в стиле Мафия - Шикарный сюрприз и
            Незабываемый День Рождения. Искрящейся, Сияющей, Веселой и
            Очаровательной Светланы!!!
          </p>
        </div>
        <div className="section_comments__carousel-item carousel-item">
          <img src={icon3} alt="Светлана" className="carousel-item__image" />
          <span className="carousel-item__name">Светлана</span>
          <p className="carousel-item__comment">
            Катенька, мне очень понравился ваш подход в игре, организация всей
            игры от начала до конца! Все было приятно и душевно.) Спасибо за
            приятный вечер!
          </p>
        </div>
        <div className="section_comments__carousel-item carousel-item">
          <img src={icon4} alt="Инесса" className="carousel-item__image" />
          <span className="carousel-item__name">Инесса</span>
          <p className="carousel-item__comment">
            Для нашей организации игра в Мафию была в новинку. Коллеги многие
            даже никогда не играли. Проводила игру у нас Екатерина, за что ей
            огромное спасибо!{" "}
            <span role="img" aria-label="cool">
              👍
            </span>{" "}
            Игра была атмосферная, интересная, активная и жаркая!{" "}
            <span role="img" aria-label="fire">
              🔥
            </span>{" "}
            Сказать, что нам понравилось? Не то слово, все в восторге и хотят
            сыграть точно не один ещё раз!{" "}
            <span role="img" aria-label="smile">
              😊
            </span>{" "}
            Огромное спасибо и творческих успехов.
          </p>
        </div>
      </Carousel>
    </Section>
  );
}
