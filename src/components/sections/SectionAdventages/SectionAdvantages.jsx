import React from "react";

import "./SectionAdvantages.scss";

import icon1 from "../../../images/advantages/icon_1.png";
import icon2 from "../../../images/advantages/icon_2.png";
import icon3 from "../../../images/advantages/icon_3.png";
import icon4 from "../../../images/advantages/icon_4.png";
import icon5 from "../../../images/advantages/icon_5.png";

import { Section } from "../Section";
import { AdvantagesItem } from "./AdventagesItem/AdvantagesItem";

export function SectionAdvantages() {
  return (
    <Section className="section_advantages">
      <h2>ПРЕИМУЩЕСТВА ИГРЫ</h2>
      <AdvantagesItem image={icon1} title="новые эмоции">
        Новые впечатления и яркие эмоции добавят разнообразие в привычные будни!
        Знакомьтесь с интересными людьми!
      </AdvantagesItem>
      <AdvantagesItem image={icon2} title="идеальный тимбилдинг">
        Бухгалтер Наталья — мафия или мирный житель? А начальник отдела — шериф
        или глава головорезов? Попробуйте угадать!
      </AdvantagesItem>
      <AdvantagesItem image={icon3} title="антураж вечера">
        Шляпы, пистолеты и стопки банкнот! Ощути себя настоящим гангстером. На
        память останутся шикарные фотографии!
      </AdvantagesItem>
      <AdvantagesItem image={icon4} title="ИГРАТЬ МОЖЕТ КАЖДЫЙ">
        Новичку понадобится всего 10 минут, чтобы разобраться в правилах игры, и
        влиться в процесс! В мафию играют взрослые и дети старше 11 лет.
      </AdvantagesItem>
      <AdvantagesItem image={icon5} title="тренировка навыков">
        Распознаете ложь? Легко блефуете? Мафия учит разбираться в психологии
        поведения, прокачивает логическое мышление и актёрские навыки.
      </AdvantagesItem>
      <div className="section_advantages__action-block">
        <h3>уже интересно?</h3>
        <p>оставь заявку на консультацию!</p>
        <a className="link section_advantages__link" href="#section_form">
          Оставить заявку
        </a>
      </div>
    </Section>
  );
}
