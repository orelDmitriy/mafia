import React from "react";

import "./AdvantagesItem.scss";

export function AdvantagesItem({ image, title, children }) {
  return (
    <div className="advantages-item">
      <img src={image} alt={title} className="advantages-item__image" />
      <h3 className="advantages-item__title">{title}</h3>
      <p className="advantages-item__text">{children}</p>
    </div>
  );
}
