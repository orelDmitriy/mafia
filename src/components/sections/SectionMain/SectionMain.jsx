import React from "react";
import arrowDown from "../../../images/arrow_down.png";

import { Section } from "../Section";

import "./SectionMain.scss";

export function SectionMain() {
  return (
    <Section className="section_main">
      <h1>«МАФИЯ»</h1>
      <h2>увлекательная ИГРА С ДЕТЕКТИВНЫМ СЮЖЕТОМ</h2>
      <p>Организуем отдых для вашей компании!</p>
      <a className="link section_main__link" href="#section_form">
        Оставить заявку
        <img src={arrowDown} alt="оставить заявку" className="link__arrow" />
      </a>
    </Section>
  );
}
