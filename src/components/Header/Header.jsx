import React from "react";
import "./Header.scss";

import { Logo } from "../Logo/Logo";
import { Phone } from "../Phone/Phone";
import { Navigate } from "../Navigate/Navigate";

export function Header() {
  return (
    <header className="header">
      <Logo className="header__logo" />
      <Phone className="header__phone" number="375336666363" />
      <Navigate className="header__navigate" />
    </header>
  );
}
