import React, { Component } from "react";

export class Carousel extends Component {
  render() {
    const { children } = this.props;
    return <div>{children}</div>;
  }
}
