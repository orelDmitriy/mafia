import React from "react";

import { Header } from "../Header/Header";
import { Footer } from "../Footer/Footer";
import { Main } from "../Mian/Main";

import "./App.scss";

export function App() {
  return (
    <div className="app">
      <Header />
      <Main />
      <Footer />
    </div>
  );
}
