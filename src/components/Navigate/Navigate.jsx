import React from "react";
import "./Navigate.scss";

import { concatClasses } from "../../utils/utils";

export function Navigate({ className }) {
  return <nav className={concatClasses("navigate", className)}>navigate</nav>;
}
