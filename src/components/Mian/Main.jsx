import React from "react";
import { SectionMain } from "../sections/SectionMain/SectionMain";
import { SectionAdvantages } from "../sections/SectionAdventages/SectionAdvantages";
import { SectionGameTypes } from "../sections/SectionGameTypes/SectionGameTypes";
import { SectionComments } from "../sections/SectionComments/SectionComments";
import { SectionPrices } from "../sections/SectionPrices/SectionPrices";
import { SectionAbout } from "../sections/SectionAbout/SectionAbout";
import { SectionForm } from "../sections/SectionForm/SectionForm";
import { SectionQuestions } from "../sections/SectionQuestions/SectionQuestions";

export function Main() {
  return (
    <main className="main">
      <SectionMain />
      <SectionAdvantages />
      <SectionGameTypes />
      <SectionComments />
      <SectionPrices />
      <SectionAbout />
      <SectionQuestions />
      <SectionForm />
    </main>
  );
}
