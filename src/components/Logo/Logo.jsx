import React from "react";
import logo from "../../images/logo.png";

import "./Logo.scss";

import { concatClasses } from "../../utils/utils";

export function Logo({ className }) {
  return (
    <img
      className={concatClasses("logo", className)}
      src={logo}
      alt="Клуб Мафии - Patron"
    />
  );
}
