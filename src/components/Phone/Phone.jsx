import React from "react";
import "./Phone.scss";

import { concatClasses } from "../../utils/utils";

export function Phone({ number, className }) {
  return (
    <div className={concatClasses("phone", className)}>
      <a href={`tel+${number}`}>
        <span className="phone__country-code">+{number.slice(0, 3)}</span>
        <span className="phone__operator-code">({number.slice(3, 5)})</span>
        <span className="phone__number">
          {number.slice(5, 8)}-{number.slice(8, 10)}-{number.slice(10)}
        </span>
      </a>
    </div>
  );
}
