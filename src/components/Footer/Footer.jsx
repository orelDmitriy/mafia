import React from "react";
import { Logo } from "../Logo/Logo";
import { Phone } from "../Phone/Phone";

export function Footer() {
  return (
    <footer className="footer">
      <address>
        клуб игры в мафию «PATRON» минск, ул. гурского, д.43а, joys cafe
        <Logo />
        контакты:
        <Phone className="footer__phone" number="375446663636" />
      </address>
      <div className="footer__copy">
        © 2018-2020 mafiapatron.by Все права защищены.
      </div>
    </footer>
  );
}
